/*
 How do we display the following task in the console>

1. drink html
2. eat javascript
3. inhale css
4. bake bootstrap

*/

/*
arrays - used to store multiple related data values inside a single variable. Arrays are declared using square bracket []

SYNTAX:
	let/const <arrayName> = ["elementA", "elementB",..."elementN"];

arrays are used if there is a need to manipulate the related values stored in it.

index - position of each element in the array.Calling an element through the index is determined by using this syntax:
	arrayName[index]

REMINDER: the index is always starting with 0. counting the elements inside would start with 0 instead of 1
	formula: nth element -1/ arrayName.length -1
		-start counting from 0
*/

tasks= ["drink html","eat javascript", "inhale css","bake bootstrap"];
console.log(tasks);
console.log(tasks[2]); //access the specific array

/*let indexOfLastElement = tasks.length -1;
console.log(indexOfLastElement)*/

console.log(tasks.length) //getting the number of element

console.log(tasks[4]) // accessing the index of non-existing element would result to undefined

//==============================================================
//Array Manipluation

//manipulating the end of the array
//ADDING AN ELEMENT
let numbers = ["one", "two", "three", "four"];
console.log(numbers)
// console.log(numbers[4]);

//using assignment operator
numbers[4] = "five"
console.log(numbers);

//Push Method
numbers.push("element") // add an element/s at the end of the array
console.log(numbers)

//callback function - a function that is passed on (or inserted) to another function. this is done because the inserted function is following a particular syntax and the developer is trying to simplify that syntax by just inserting it inside another function
function pushMethod (element){
	numbers.push(element);
}
pushMethod("six");
pushMethod("seven");
pushMethod("eight");
console.log(numbers);

// REMEMOVING OF AN ELEMENT
//pop method - removes the element at the end of the array (last element);
numbers.pop();
console.log(numbers);

function popMethod(){
	numbers.pop()
}
popMethod();
console.log(numbers);

//==========================================
//manipulating the beginning/start of the array
//remove the first element
//shift method - remove the first element of the array
numbers.shift();
console.log(numbers);

//callback function
function shiftMethod (){
	numbers.shift()
}
shiftMethod();
console.log(numbers);


// ======================
// ADDING AN ELEMENT
//unshift method
numbers.unshift("zero");
console.log(numbers);

// callback function
function unshiftMethod(element){
	numbers.unshift(element)
;}
unshiftMethod("mcdo");
// unshiftMethod(1);
console.log(numbers);

// Arrangement of the elements

let numbs = [15, 27, 32, 12, 6, 8, 236]
console.log(numbs);
//sort method - arranges the elements in ascending or descending. it has an anonymous function inside that has 2 parameters
	// anonymous function - unnamed function and can only be used once

/*
	2 parameters inside the anonymous function represents:
	 first parameter - first/smallest/starting element
	 second parameter - last/biggest/end element
*/

/*
	SYNTAX:
		arrayName.sort(
		 function (a,b){
			a - b - ascending
			b - a - descending
		 }
		);

*/

//  ascending order
numbs.sort(
	function(a,b){
		return a- b
	}
);
console.log(numbs);

// descending order
numbs.sort(
	function(a,b){
		return b - a
	}
);
console.log(numbs);

//reverse method - reverses the order of the elements in an array. it will depend on the last arrangement of the array, regardless if it is ascending, descending have in random order.
numbs.reverse()
console.log(numbs)

// =================================
// splice method - ctrl x + ctrl p

/*
	-directly manipulates the array
	-first parameter (1) - the index of the element from which the omitting will begin.
	-second parameter - determines the number of elements to be omitted
	-third paramneter onwards - the replacements for the removed elements
	SYNTAX:
		let/const <newArray> = <orignalArrayName>.splice(firstParameter, secondParameter, thirdParameter,...nthParameter)

*/


// one parameter(pure omission)
// let nums = numbs.splice(1);

// two parameters(pure omission)
// let nums = numbs.splice(0,2);

// three or more parameters(replacements)
let nums = numbs.splice(4, 2, 31, 11, 111);
console.log(numbs);
console.log(nums);

// ======================================
// slice method - ctrl c +ctrl p
/*
	- does not affect the original array. creates a sub-array, but does not omit any element from the original array
	- first parameter - the index where copying will begin
	- second parameter - the number of elements to be coped starting from the first element (copying will still begin at the first parameter)
		SYNTAX:
		let/const <newArray> = <orignalArrayName>.splice(firstParameter, secondParameter, thirdParameter,...nthParameter)
*/

// one parameter
// let slicedNums = numbs.slice(1);


// two parameter
let slicedNums = numbs.slice(2, 7);
console.log(numbs);
console.log(slicedNums);

// ============
// merging of array
// Concat
console.log(numbers);
console.log(numbs);
let animals = ["dog", "tiger", "kangaroo", "chicken"]
console.log(animals);

let newConcat = numbers.concat(numbs, animals);
console.log(newConcat);
console.log(numbers);
console.log(numbs);
console.log(animals);

// join method
// parameters - seperators

let meal = ["rice", "steak", "juice"]
console.log(meal);

newJoin= meal.join("");
console.log(newJoin)

newJoin= meal.join(" ");
console.log(newJoin)

newJoin= meal.join("-");
console.log(newJoin)

// toString method
console.log(nums);
// typeof determines the data type of the element after it
console.log(typeof nums);

/*
	toString - converts the element into string data type
	parseInt - converts string into element

*/
let newString = nums.toString();
console.log(typeof newString);console.log(newString);


// Accessors
let countries = ["US", "PH", "JP", "HK", "SG","PH", "NZ"];
// indexOf - returns the first index it finds from the begininng of the array
/*
	SYNTAX
	arrayName.indexOf()
*/
let index = countries.indexOf("PH")
console.log(index);
// finding a non -existing element
index = countries.indexOf("AU")
console.log(index);

// lastIndexOf() - finds the index of the element starting from the end of the array
index = countries.lastIndexOf("PH");
console.log(index);


// returns -1 for non-existing elements
index = countries.lastIndexOf("Ph");
console.log(index);

/* Using selection contrl structure

if(countries.indexOf("PH") === -1){
	console.log("Element not existing");

} else{
	console.log("Element exists in the array");
}*/

// Iterators
// for each


let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
console.log(days)

// for each
/*
array.forEach(
	function(element){
		statement/s
	}
)
*/


days.forEach(
		function(element){
			console.log(element)}
	)

// map
/*
	array.map(
		function(element){
			statement/s
		}
	)
*/
	// returns a copy of an array from the original which can be manipulated

let mapDays = days.map(
	function(element){
		return `${element} is the day of the week}`
	}
)
console.log(mapDays);
console.log(days);

// filter - filters the elements and copies them into another array
console.log (numbs);
let newFilter = numbs.filter(
		function (element) {
			return element < 50;
		}
)
console.log(newFilter);
console.log(numbs);

// every - checks if all the element pass the condition (returns true if all of them pass).
console.log(nums);
let newEvery = nums.every(
		function (element){
			return (element > 10);
		}
	)
console.log(newEvery);


// some - checks if atleast 1 element passes the condition
let newSome = nums.some(
		function(element){
			return ( element > 30 );
		}
	)
console.log(newSome);


// UPDATES TO FOLLOW
nums.push(50)
console.log(nums);
// reduce - preforms the operation in all of the elements in the array
// first parameter - first element
// second parameter - last element
 let newReduce = nums.reduce(
 		function ( a, b ) {
 			return a + b
 			// return b - a
 		}
 	)
console.log(newReduce);

let average = newReduce/nums.length
console.log(average)


// toFixed - sets the number of decimal places
console.log (average.toFixed(2));

/*
parseInt - rounds the number to the nearest whole number
parseFloat - rounds the number to the nearest target decimal place (through the use of .toFixed)
*/
console.log (parseInt(average.toFixed(2)));
console.log (parseFloat(average.toFixed(2)));
